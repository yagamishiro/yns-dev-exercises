<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title></title>
  </head>
  <style>
    .tbl_css {
        width: 90%;
        margin: auto;
        margin-top: 3%;
        margin-bottom: 20px;
    }
  </style>
<body>
    <?php
        include 'database_conn.class.php';
        $connect = new DatabaseConnection('127.0.0.1','root','','7-4');
        $conn = $connect->connect();

        $sql = "SELECT id,
                CASE
                    WHEN parent_id IS NULL THEN 'NULL'
                    ELSE parent_id
                END AS p 
                FROM table_master_data
                ORDER BY
                    CASE 
                        WHEN `parent_id` IS NULL THEN id
                        ELSE `parent_id`
                    END
                ASC
                ";
        $result = $conn->query($sql);
        ?>
            <h1 class="text-center mt-3">OUTPUT</h1>
            <table class="table table-bordered tbl_css">
                <thead>
                    <tr>
                        <th scope="col">id</th>
                        <th scope="col">parent_id</th>
                    </tr>
                </thead>
                <tbody>
        <?php
            if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) { ?>
                    <tr>
                        <td><?=$row['id']?></td>
                        <td><?=$row['p']?></td>
                    </tr>
            <?php
            }
        }
    ?>
                </tbody>
            </table>
</body>
</html>