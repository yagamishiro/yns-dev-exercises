<!DOCTYPE html>
<html>
<body>

<h2>JavaScript Confirm Box</h2>

    <input type="number" id="number" />
    <button type="button" onclick="findPrime()">Press to display Prime Numbers</button>

    <h1 id="result"></h1>

<script>
function findPrime() {
    const maxNumber = parseInt(document.getElementById("number").value);
    let arr = [];

    for (let i = 2; i <= maxNumber; i++) {
        let flag = 0;

        for (let j = 2; j < i; j++) {
            if (i % j == 0) {
                flag = 1;
                break;
            }
        }
        if (i > 1 && flag == 0) {
            arr.push(i);
        }
    }
    const res = arr.join(', ');
    document.getElementById("result").innerHTML = "Prime Numbers from 2 to " + 
    maxNumber + " are : <br>" + "[ " + res + " ]";
}
</script>

</body>
</html>
