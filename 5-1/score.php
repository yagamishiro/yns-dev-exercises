<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <title></title>
    <style>
        .userName {
            width: 40%;
            margin: auto;
            text-align: center;
            margin-top: 10%;
        }
        .labelName {
            font-size: 20px;
        }
        .submitBtn {
            width: 20%;
            margin-left: 40%;
            margin-top: 7%;
        }
        .bs {
            margin-top: 7%;
        }
    </style>
  </head>
  <body>
        <div style="width: 100%; margin: auto; padding: 10px;" class="mt-5">
            <h1 class="text-center">CONGRATULATIONS!</h1>
            <h6 class="text-center">You have finished the quiz.</h6>
            
            <h2 class="text-center mt-5">
                FINAL SCORE : <br>
            </h2>
            <h1 class="text-center">
            <?php
                session_start();
                if (isset($_SESSION['score'])) {
                    
                    echo $_SESSION['score'];
                    unset($_SESSION['score']);
                    session_destroy();
                }
            ?>
            </h1>

            <h2 class="text-center bs">BEST SCORE : </h2>
            <h1 class="text-center">
            <i class="fas fa-crown"></i>
            <?php
                $servername = "127.0.0.1";
                $username = "root";
                $password = "";
                $dbname = "test";
            
                // Create connection
                $conn = new mysqli($servername, $username, $password, $dbname);
                // Check connection
                if ($conn->connect_error) {
                    die("Connection failed: " . $conn->connect_error);
                }
            
                $sql = "SELECT * FROM scores WHERE score = (SELECT MAX(score) FROM scores) ORDER BY date_created DESC LIMIT 1 ";

                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                        echo $row['name']. " - " .$row['score'];
                    }
                }
            ?>
            </h1>
            <form method="POST" action="action.php">
                <button type="submit" class="btn btn-success submitBtn" name="finishBtn">Finish</button>
            </form>
        </div>
  </body>
</html>