<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title></title>
    <style>
        .userName {
            width: 40%;
            margin: auto;
            text-align: left;
            margin-top: 2%;
        }
        .labelName {
            font-size: 20px;
        }
        .submitBtn {
            width: 20%;
            margin-left: 59%;
            margin-top: 1%;
        }
        .addQuestion {
            display:none;
        }
        .addBtn {
            margin-left: 90%;
        }
    </style>
  </head>
  <body>
        <div style="width: 100%; margin: auto; padding: 10px;" class="mt-3">
            <h1 class="text-center">LIST OF QUESTIONS</h1>
            <div class="addQuestion" id="addQuestion">
                <form method="POST" action="action.php">
                    <div class="form-group userName">
                        <label for="userName" class="labelName">Enter new question: </label>
                        <input type="text" class="form-control" name="question">
                    </div>
                    <div class="form-group userName">
                        <label for="userName" class="labelName">Enter answer choice 1: </label>
                        <input type="text" class="form-control" name="choiceOne">
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input cb" id="exampleCheck1" name="rightAnswerCB" value="one" onchange="cbChange(this)">
                                <label class="form-check-label" for="exampleCheck1">Mark as right answer</label>
                            </div>
                    </div>
                    <div class="form-group userName">
                        <label for="userName" class="labelName">Enter answer choice 2: </label>
                        <input type="text" class="form-control" name="choiceTwo">
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input cb" id="exampleCheck1" name="rightAnswerCB" value="two" onchange="cbChange(this)">
                                <label class="form-check-label" for="exampleCheck1" name="rightAnswer">Mark as right answer</label>
                            </div>
                    </div>
                    <div class="form-group userName">
                        <label for="userName" class="labelName">Enter answer choice 3: </label>
                        <input type="text" class="form-control" name="choiceThree">
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input cb" id="exampleCheck1" name="rightAnswerCB" value="three" onchange="cbChange(this)">
                                <label class="form-check-label" for="exampleCheck1" name="rightAnswer">Mark as right answer</label>
                            </div>
                    </div>
                    <div class="submitBtn">
                        <button type="submit" class="btn btn-success" name="addQuestion">Submit</button>
                        <button type="button" class="btn btn-secondary" name="close" onclick="closeForm()">Close</button>
                    </div>
                </form>
            </div>

            <?php
                $servername = "127.0.0.1";
                $username = "root";
                $password = "";
                $dbname = "test";
            
                // Create connection
                $conn = new mysqli($servername, $username, $password, $dbname);
                // Check connection
                if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
                }
                
                $sql = "SELECT * FROM questions";
                $result = $conn->query($sql);
                ?>
                <div>
                <div>
                    <button type="button" class="btn btn-primary addBtn" onclick="addNewQuestion()">New question</button>
                </div>
                <table class="table table-hover mt-3">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Questions</th>
                        <th scope="col">Choice One</th>
                        <th scope="col">Choice Two</th>
                        <th scope="col">Choice Three</th>
                        <th scope="col">Correct Answer</th>
                        </tr>
                    </thead>
                <?php 
                    if ($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                ?>
                    <tbody>
                        <tr>
                        <th scope="row"><?=$row['id']?></th>
                        <td><?=$row['questions']?></td>
                        <td><?=$row['choiceOne']?></td>
                        <td><?=$row['choiceTwo']?></td>
                        <td><?=$row['choiceThree']?></td>
                        <td><?=$row['correctAnswer']?></td>
                        </tr>   
                    </tbody>        
            <?php }} ?>
                </table>
                </div> 
        </div>
  </body>
</html>
<script>
    function cbChange(obj) {
        var cbs = document.getElementsByClassName("cb");
        for (var i = 0; i < cbs.length; i++) {
            cbs[i].checked = false;
        }
        obj.checked = true;
    }
    function addNewQuestion() {
        document.getElementById("addQuestion").style.display = "block";
    }
    function closeForm() {
        document.getElementById("addQuestion").style.display = "none";
    }
</script>