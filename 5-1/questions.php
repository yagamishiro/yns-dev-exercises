<?php
    session_start();

    $servername = "127.0.0.1";
    $username = "root";
    $password = "";
    $dbname = "test";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "SELECT * FROM questions ORDER BY Rand() LIMIT 10";
    $result = $conn->query($sql);
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title></title>
    <style>
        .userName {
            width: 40%;
            margin: auto;
            text-align: center;
            margin-top: 10%;
        }
        .labelName {
            font-size: 20px;
        }
        .submitBtn {
            width: 20%;
            margin-left: 20%;
            margin-top: 7%;
        }
        .questions {
            /* border: 1px solid black; */
            padding: 50px;
            margin-left: 25%;
        }
        .textFont {
            font-size: 20px;
        }
        .rb {
            margin-left: 50px;
        }
    </style>
  </head>
  <body>
        <div style="width: 100%; margin: auto; padding: 10px;" class="mt-5">
            <h1 class="text-center">Questions and Answers</h1>
            <div class="questions">
            <form method="POST" action="action.php">
            <?php
                $ctr = 0;
                if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                $ctr++;
            ?>
            <div class="row">
                <div class="col-0">
                    <label class="col-sm-2 col-form-label textFont"><?=$ctr?>.</label>
                </div>
                <div class="col">
                    <p class="form-control-plaintext textFont"><?=$row['questions']?></p>
                </div>
            </div>
            <fieldset class="form-group rb">
                <div class="row">
                <div class="col-sm-10">
                    <div class="form-check">
                        <input class="form-check-input textFont" type="radio" name="ans<?=$ctr?>"
                        id="gridRadios1" value="<?=$row['choiceOne']?>">
                        <label class="form-check-label textFont" for="<?=$row['id']?>">
                            <?=$row['choiceOne']?>
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input textFont" type="radio" name="ans<?=$ctr?>"
                        id="gridRadios2" value="<?=$row['choiceTwo']?>">
                        <label class="form-check-label textFont" for="<?=$row['id']?>">
                            <?=$row['choiceTwo']?>
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input textFont" type="radio" name="ans<?=$ctr?>"
                        id="gridRadios2" value="<?=$row['choiceThree']?>">
                        <label class="form-check-label textFont" for="<?=$row['id']?>">
                            <?=$row['choiceThree']?>
                        </label>
                    </div>
                    <div class="form-check">
                        <input hidden class="form-check-input textFont" type="text" name="cor<?=$ctr?>"
                        id="gridRadios2" value="<?=$row['correctAnswer']?>">
                    </div>
                </div>
                </div>
            </fieldset>
            <?php }} ?>
                <button type="submit" class="btn btn-primary submitBtn" name="submitQuiz">Submit</button>
            </form>
            </div>
        </div>
  </body>
</html>