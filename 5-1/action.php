<?php
    session_start();
    $servername = "127.0.0.1";
    $username = "root";
    $password = "";
    $dbname = "test";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    if (isset($_POST['submit'])) {
        
        $_SESSION['userName'] = $_POST['userName'];

        if ($_SESSION['userName'] == '') {
            echo "Enter a name to proceed.";
        } else {
            header("Location:http://127.0.0.1:8080/5-1/questions.php");
        }
    }

    if (isset($_POST['addQuestion'])) {
        $question = $_POST['question'];
        $choiceOne = $_POST['choiceOne'];
        $choiceTwo = $_POST['choiceTwo'];
        $choiceThree = $_POST['choiceThree'];
        $noError = true;
        
        if ($question == '' || $choiceOne == '' || $choiceTwo == '' || $choiceThree == '') {
            echo "Fields must not be empty.";
            $noError = false;
        }

        switch ($_POST['rightAnswerCB']) {
            case 'one':
                $correctAnswer = $_POST['choiceOne'];
                break;
            case 'two':
                $correctAnswer = $_POST['choiceTwo'];
                break;
            case 'three':
                $correctAnswer = $_POST['choiceThree'];
                break;
            default:
                echo "Error.";
        }
        if ($noError) {
            $sql = "INSERT INTO questions (questions, choiceOne, choiceTwo, choiceThree,
            correctAnswer)
            VALUES ('$question','$choiceOne','$choiceTwo','$choiceThree','$correctAnswer')";

            if (mysqli_query($conn, $sql)) {
                echo "Question successfuly added.";
                // $_SESSION['error'] = "Account succesfully created.";
                // header("Location:http://localhost/yns-dev-exercises/3-5/index.php");
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
                // $_SESSION['error'] = "Error: " . $sql . "<br>" . $conn->error;;
                // header("Location:http://localhost/yns-dev-exercises/3-5/index.php");
            }
        }

    }

    if (isset($_POST['submitQuiz'])) {
        $score = 0;
        for ($i=1;$i<=10;$i++) {
            if ($_POST['ans'.$i] == $_POST['cor'.$i]) {
                $score++;
            }
        }
        $score = $score * 10;

        $sql = "INSERT INTO scores (`name`, score, date_created)
            VALUES ('$_SESSION[userName]','$score',NOW())";

            if (mysqli_query($conn, $sql)) {
                $_SESSION['score'] = $score;
                header("Location:http://127.0.0.1:8080/5-1/score.php");
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
                // $_SESSION['error'] = "Error: " . $sql . "<br>" . $conn->error;;
                // header("Location:http://localhost/yns-dev-exercises/3-5/index.php");
            }
    }

    if (isset($_POST['finishBtn'])) {
        session_start();
        header("Location:http://127.0.0.1:8080/5-1/index.php");
        session_destroy();
    }
?>