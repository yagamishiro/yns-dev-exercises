<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title></title>
    <style>
        .userName {
            width: 40%;
            margin: auto;
            text-align: center;
            margin-top: 10%;
        }
        .labelName {
            font-size: 20px;
        }
        .submitBtn {
            width: 20%;
            margin-left: 40%;
            margin-top: 7%;
        }
    </style>
  </head>
  <body>
        <div style="width: 100%; margin: auto; padding: 10px;" class="mt-5">
            <h1 class="text-center">WELCOME TO THE QUIZ HOMEPAGE</h1>
            <h5 class="text-center">Let us test your limit</h5>
            
            <form method="POST" action="action.php">
                <div class="form-group userName">
                    <input type="text" class="form-control" name="userName">
                    <label for="userName" class="labelName">Enter your name</label>
                </div>
                <button type="submit" class="btn btn-primary submitBtn" name="submit">Submit</button>
            </form>
        </div>
  </body>
</html>