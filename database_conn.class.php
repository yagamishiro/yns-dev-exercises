<?php
    class DatabaseConnection {
        protected $conn;
        private $host;
        protected $user;
        protected $password;
        protected $dbName;
        
        function __construct($host,$user,$password,$dbName) {
            $this->host = $host;
            $this->user = $user;
            $this->password = $password;
            $this->dbName = $dbName;
        }

        public function connect() {
            $this->conn = null;

            $this->conn = new mysqli($this->host, $this->user, $this->password, $this->dbName);

            if ($this->conn->connect_error) {
                return die("Connection failed: " . $this->conn->connect_error);
            }
            return $this->conn;
        }
    }
?>