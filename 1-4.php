<html>
    
    <body>
        <form method="POST">
        <input type="number" name="numberOne" placeholder="Input a number"/>
        <input type="submit" name="calculate" value="Submit" />
        </form>
    </body>

    <?php
        if (isset($_POST['calculate'])) {
            $numberOne = $_POST['numberOne'];
            
            for($i = 1; $i <= $numberOne; $i++)
            {
                if ($i % 3 == 0 && $i % 5 == 0) {
                    echo 'FizzBuzz<br>';
                } elseif ($i % 3 == 0) {
                    echo 'Fizz<br>';
                } elseif ($i % 5 == 0) {
                    echo 'Buzz<br>';
                } else {
                    echo $i . '<br>';
                }
            }
        }
    ?>

</html>