<?php
    session_start();
    if (isset($_POST['submit'])) {
        
        $email = $_POST['email'];
        $password = $_POST['password'];
        $noError = true;
        $newCsv = array();
        $filename = "../formdata.csv";
        if ($email == '' || $password == '') {
            $_SESSION['error'] = "You left one or more of the required fields.";
            header("Location:/yns-dev-exercises/1-13/index.php");
        }
        if (file_exists($filename)) {
            $handle = fopen($filename, "r");
            while ($csvcontents = fgetcsv($handle)) {
                array_push($newCsv, $csvcontents);
            }
            // print_r($new_csv);
            foreach ($newCsv as $key => $row) {
                if ($row[8] == $email && $row[9] == $password) {
                    $_SESSION['error'] = "Access Granted.";
                    $_SESSION['logged_in'] = true;
                    header("Location: /yns-dev-exercises/1-13/profile.php");
                } else {
                    echo 'errror';
                    $_SESSION['error'] = "Sorry, that email or password is incorrect. Please try again.";
                    header("Location: /yns-dev-exercises/1-13/index.php");
                }
            }
            // echo '<pre>'; print_r(array_slice($new_csv, 0, 10)); echo '</pre>';
            fclose($handle);
        }
    }

    if (isset($_POST['create_account'])) {
        $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $middlename = $_POST['middlename'];
        $gender = $_POST['gender'];
        $contactNumberExt = $_POST['contact_number_ext'];
        $contactNumber = $_POST['contact_number'];
        // $fullContactNumber = $contactNumberExt + $contactNumber;
        $email = $_POST['email'];
        $birthday = $_POST['birthday'];
        $address = $_POST['address'];
        $password = $_POST['password'];
        $confirmPassword = $_POST['confirmPassword'];
        $image = '';
        $noError = true;

        $targetDir = "../uploads/";
        $targetFile = $targetDir . basename($_FILES["fileToUpload"]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($targetFile,PATHINFO_EXTENSION));

        // Validate if fields required are empty
        if (
            $firstname == '' ||
            $lastname == '' ||
            $email == '' ||
            $birthday == '' ||
            $address == '' ||
            $contactNumber == '' ||
            $password == '' ||
            $confirmPassword == ''
        ) {
            $_SESSION['error'] = "You left one or more of the required fields.";
            header("Location:/yns-dev-exercises/1-13/create-account.php");
            $noError = false;
        } elseif (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $email)) {
            $_SESSION['error'] = "Email entered is not valid.";
            header("Location:/yns-dev-exercises/1-13/create-account.php");
            $noError = false;
        } elseif (!preg_match('/^[0-9]{10}+$/', $contactNumber)) {
            $_SESSION['error'] = "Contact Number is not valid.";
            header("Location:/yns-dev-exercises/1-13/create-account.php");
            $noError = false;
        }

        if ($password != $confirmPassword) {
            $_SESSION['error'] = "Password do not match.";
            header("Location:/yns-dev-exercises/1-13/create-account.php");
            $noError = false;
        }

        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        if($check !== false) {
            $uploadOk = 1;
        } else {
            echo "File is not an image.";
            $uploadOk = 0;
        }

        // Check file size
        if ($_FILES["fileToUpload"]["size"] > 500000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }

        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $targetFile)) {
                $image = htmlspecialchars( basename( $_FILES["fileToUpload"]["name"]));
            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }

        // Display inputted data
        if ($noError) {
            $header = "Image,Firstname,Lastname,Middlename,Gender,Contact Number,Birthday,Address,Email,Password\n";
            $data = "$image,$firstname,$lastname,$middlename,$gender,$contactNumber,$birthday,$address,$email,$password\n";
            $filename = dirname(__DIR__) . "/formdata.csv";

            if (file_exists($filename)) {
                file_put_contents($filename, $data, FILE_APPEND);
                $_SESSION['error'] = "Successfully saved to CSV.";
                header("Location:/yns-dev-exercises/1-13/index.php");
            } else {
                file_put_contents($filename, $header . $data);
                $_SESSION['error'] = "Successfully saved to CSV.";
                header("Location:/yns-dev-exercises/1-13/index.php");
            }
        }
    }

    if (isset($_POST['logout'])) {
        session_start();
        header("Location:/yns-dev-exercises/1-13/index.php");
        session_destroy();
    }


?>