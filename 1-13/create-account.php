<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title></title>
  </head>
  <body>
        <form method="POST" action="action.php" enctype="multipart/form-data">
        <div style="width: 40%; margin: auto; border: 1px solid black; padding: 10px;" class="mt-5 mb-5">
            <h1>Basic User Information</h1>
            <br>
            <span style="padding: 10px; color: red;">
                <?php
                    session_start();
                    if (isset($_SESSION['error'])) {
                        echo $_SESSION['error'];
                        unset($_SESSION['error']);
                    }
                ?>
            </span>
            <br>
            <div class="form-group">
            <label>Profile Picture : </label>
            <input type="file" class="form-control" name="fileToUpload" id="fileToUpload">
            </div>

            <div class="form-group">
            <label>Lastname : </label>
            <input type="text" class="form-control" name="lastname" placeholder="Input your lastname"/>
            </div>

            <div class="form-group">
            <label>Firstname : </label>
            <input type="text" class="form-control" name="firstname" placeholder="Input your firstname"/>
            </div>

            <div class="form-group">
            <label>Middlename(Optional) : </label>
            <input type="text" class="form-control" name="middlename" placeholder="Input your middlename"/>
            </div>

            <div class="form-group">
            <label>Gender : </label>
            <select class="form-control" name="gender">
                <option value="Male">Male</option>
                <option value="Female">Female</option>
            </select>
            </div>

            <div class="form-group">
            <label>Contact Number : </label>
            <div class="input-group">
            <input readonly class="form-control col-2" type="text" name="contact_number_ext" value="+63"/>
            <input type="text" class="form-control" name="contact_number" placeholder="Input your contact number"/>
            </div>
            </div>

            <div class="form-group">
            <label>Birthday : </label>
            <input type="date" class="form-control" name="birthday" placeholder="Input your birthday"/>
            </div>

            <div class="form-group">
            <label>Address : </label>
            <input type="text" class="form-control" name="address" placeholder="Input your address"/>
            </div>

            <div class="form-group">
            <label>Email : </label>
            <input type="text" class="form-control" name="email" placeholder="Input your email address"/>
            </div>

            <div class="form-group">
                <label for="pwd">Password:</label>
                <input type="password" class="form-control" placeholder="Enter password" name="password">
            </div>

            <div class="form-group">
                <label for="pwd">Confirm Password:</label>
                <input type="password" class="form-control" placeholder="Enter password" name="confirmPassword">
            </div>

            <div class="form-group">
            <input class="btn btn-primary" type="submit" name="create_account" value="Submit" />
        </div>
        </form>
  </body>
</html>