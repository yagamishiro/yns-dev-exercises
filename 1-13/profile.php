<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title></title>
  </head>
<body>
    <?php 
        session_start();
        if (! empty($_SESSION['logged_in'])) {
    ?>
    <h1 class="text-center mt-5">User Profile</h1>
    <?php
        $csv = array();
        $newCsv = array();
        $filename = "../formdata.csv";
        $header = true;
        echo '<table class="table table-bordered">';
        if (file_exists($filename)) {
            $handle = fopen($filename, "r");
            while ($csvcontents = fgetcsv($handle)) {
                $csv = $csvcontents;
                array_push($newCsv, $csv);
            }
            ?>
            <div class="container" style="width: 100%; border: 1px solid grey; border-radius: 30px; padding: 10px; overflow: hidden;">
                <div class="profileImage" style="float: left;">
                    <div style="margin-left:40%;">
                        <img src=../uploads/<?=$newCsv[1][0]?> width=230 height=230 style="border-radius: 50%;">
                    </div>
                </div>
                <div class="userInfo">
                    <div style="margin-left:40%;">
                        <label style="font-size: 20px;">Name : </label>
                        <label style="font-weight: bold;"><?=$newCsv[1][1] . " " . $newCsv[1][3] . ". " . $newCsv[1][2]?></label>
                    </div>
                    <div style="margin-left:40%;">
                        <label style="font-size: 20px;">Gender : </label>
                        <label style="font-weight: bold;"><?=$newCsv[1][4]?></label>
                    </div>
                    <div style="margin-left:40%;">
                        <label style="font-size: 20px;">Birthday : </label>
                        <label style="font-weight: bold;"><?=$newCsv[1][6]?></label>
                    </div>
                    <div style="margin-left:40%;">
                        <label style="font-size: 20px;">Address : </label>
                        <label style="font-weight: bold;"><?=$newCsv[1][7]?></label>
                    </div>
                    <div style="margin-left:40%;">
                        <label style="font-size: 20px;">Contact Number : </label>
                        <label style="font-weight: bold;">0<?=$newCsv[1][5]?></label>
                    </div>
                    <div style="margin-left:40%;">
                        <label style="font-size: 20px;">Email Address : </label>
                        <label style="font-weight: bold;"><?=$newCsv[1][8]?></label>
                    </div>
                </div>
            </div>
            <?php fclose($handle);
        }?>
        <br>
        <form method="POST" action="action.php">
            <button type="submit" class="btn btn-primary" name="logout" style="margin-left:48%;">Logout</button>
        </form>
        <?php } else { ?>
            <h3 class="text-center mt-5">You are not logged in. <a href="index.php">Click here</a> to log in.</h3>
        <?php } ?>
</body>
</html>