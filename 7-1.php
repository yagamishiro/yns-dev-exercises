<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title></title>
  </head>
  <style>
    .tbl_css {
        width: 90%;
        margin: auto;
        margin-top: 3%;
    }
  </style>
<body>
    <?php
        include 'database_conn.class.php';
        $connect = new DatabaseConnection('127.0.0.1','root','','sql_problems');
        $conn = $connect->connect();

        $sql = "SELECT *, TIMESTAMPDIFF(YEAR,birth_date,CURDATE()) AS age
        FROM employees 
        WHERE TIMESTAMPDIFF(YEAR,birth_date,CURDATE()) > 30 
        AND TIMESTAMPDIFF(YEAR,birth_date,CURDATE()) < 40 
        ORDER BY age";
        $result = $conn->query($sql);
        ?>
            <h1 class="text-center mt-3">Employees older than 30 years old but younger than 40 years old</h1>
            <table class="table table-bordered tbl_css">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">First Name</th>
                        <th scope="col">Middle Name</th>
                        <th scope="col">Last Name</th>
                        <th scope="col">Birthdate</th>
                        <th scope="col">Age</th>
                    </tr>
                </thead>
                <tbody>
        <?php
            $ctr = 1;
            if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) { ?>
                    <tr>
                        <th scope="row"><?=$ctr?></th>
                        <td><?=$row['first_name']?></td>
                        <td><?=$row['middle_name']?></td>
                        <td><?=$row['last_name']?></td>
                        <td><?=$row['birth_date']?></td>
                        <td><?=$row['age']?></td>
                    </tr>
            <?php
            $ctr++;
            }
        }
    ?>
                </tbody>
            </table>
</body>
</html>