<!DOCTYPE html>
<html>
<body>

<h1>Please write something.</h1>

<input type="text" id="userInput" oninput="displayValue()">

<h3 id="result"></h3>

<script>
function displayValue() {
  let updatedText = document.getElementById("userInput").value;
  document.getElementById("result").innerHTML = "Updated Text : " + updatedText;
}
</script>

</body>
</html>

  
