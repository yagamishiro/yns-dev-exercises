<?php
    class ExerLink {

        public function exerOne() {
            for ($i = 1; $i <= 5; $i++) {
                $content .='<a class="btn btn-outline-info btn_css" 
                href="/yns-dev-exercises/1-'.$i.'.php">Exer 1 - '.$i.'</a></button>';
             }
             return $content;
        }

        public function exerOneSix() {
            for ($i = 6; $i <= 13; $i++) {
                if ($i == 9 || $i == 11 || $i == 12) {
                    $content .='<a class="btn btn-outline-info btn_css" 
                    href="/yns-dev-exercises/1-'.$i.'.php">Exer 1 - '.$i.'</a></button>';
                } else {
                    $content .='<a class="btn btn-outline-info btn_css" 
                    href="/yns-dev-exercises/1-'.$i.'">Exer 1 - '.$i.'</a></button>';
                }
             }
             return $content;
        }

        public function exerTwo() {
            for ($i = 1; $i <= 15; $i++) {
                $content .='<a class="btn btn-outline-info btn_css" 
                href="/yns-dev-exercises/2-'.$i.'.php">Exer 2 - '.$i.'</a></button>';
             }
             return $content;
        }

        public function exerThree() {
            for ($i = 4; $i <= 5; $i++) {
                $content .='<a class="btn btn-outline-info btn_css" 
                href="/yns-dev-exercises/3-'.$i.'">Exer 3 - '.$i.'</a></button>';
             }
             return $content;
        }

        public function exerFive() {
            for ($i = 1; $i <= 2; $i++) {
                $content .='<a class="btn btn-outline-info btn_css" 
                href="/yns-dev-exercises/5-'.$i.'">Exer 5 - '.$i.'</a></button>';
             }
             return $content;
        }
    }
?>