<?php
    include 'action.class.php';
    $exerLinks = new ExerLink();
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title></title>

    <style>
        .link_group {
            padding: 10px;
        }
        .btn_css {
            margin: 10px;
        }
    </style>
  </head>
  <body>
        <div style="width: 100%; margin: auto; padding: 10px;" class="mt-2">
            <h1 class="text-center">LINKS TO THE PREVIOUS EXERCISES</h1>

            <div class="links">
                <div class="link_group">
                    <?php
                        echo $exerLinks->exerOne();
                    ?>
                </div>
                <div class="link_group">
                    <?php
                        echo $exerLinks->exerOneSix();
                    ?>
                </div>
                <div class="link_group">
                    <?php
                        echo $exerLinks->exerTwo();
                    ?>
                </div>
                <div class="link_group">
                    <?php
                        echo $exerLinks->exerThree();
                    ?>
                </div>
                <div class="link_group">
                    <?php
                        echo $exerLinks->exerFive();
                    ?>
                </div>
            </div>
        </div>
  </body>
</html>