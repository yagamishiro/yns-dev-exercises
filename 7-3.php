<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title></title>
  </head>
  <style>
    .tbl_css {
        width: 90%;
        margin: auto;
        margin-top: 3%;
        margin-bottom: 20px;
    }
  </style>
<body>
    <?php
        include 'database_conn.class.php';
        $connect = new DatabaseConnection('127.0.0.1','root','','sql_problems');
        $conn = $connect->connect();

        $sql = "SELECT employees.first_name,employees.last_name,employees.middle_name,
                       positions.name,
                CASE positions.name
                    WHEN 'CEO' THEN 'Chief Executive Officer'
                    WHEN 'CTO' THEN 'Chief Technical Officer'
                    WHEN 'CFO' THEN 'Chief Financial Officer'
                    ELSE positions.name
                END AS name
                FROM employees
                    INNER JOIN employee_positions
                        ON employees.id = employee_positions.employee_id
                    INNER JOIN positions
                        ON employee_positions.position_id = positions.id
                    ";
        $result = $conn->query($sql);
        ?>
            <h1 class="text-center mt-3">Employees with position name</h1>
            <table class="table table-bordered tbl_css">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">First Name</th>
                        <th scope="col">Middle Name</th>
                        <th scope="col">Last Name</th>
                        <th scope="col">Position</th>
                    </tr>
                </thead>
                <tbody>
        <?php
            $ctr = 1;
            if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) { ?>
                    <tr>
                        <th scope="row"><?=$ctr?></th>
                        <td><?=$row['first_name']?></td>
                        <td><?=$row['middle_name']?></td>
                        <td><?=$row['last_name']?></td>
                        <td><?=$row['name']?></td>
                    </tr>
            <?php
            $ctr++;
            }
        }
    ?>
                </tbody>
            </table>
</body>
</html>