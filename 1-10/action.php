<?php
    if (isset($_POST['submit'])) {
        $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $middlename = $_POST['middlename'];
        $gender = $_POST['gender'];
        $contactNumberExt = $_POST['contact_number_ext'];
        $contactNumber = $_POST['contact_number'];
        $fullContactNumber = $contactNumberExt + $contactNumber;
        $email = $_POST['email'];
        $birthday = $_POST['birthday'];
        $address = $_POST['address'];
        $image = '';
        $noError = true;

        $targetDir = "../uploads/";
        $targetFile = $targetDir . basename($_FILES["fileToUpload"]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($targetFile,PATHINFO_EXTENSION));
        // Validate if fields required are empty
        if (
            $firstname == '' ||
            $lastname == '' ||
            $email == '' ||
            $birthday == '' ||
            $address == '' ||
            $contactNumber == ''
        ) {
            echo "Opps! Fields should not be empty.";
            $noError = false;
        } elseif (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $email)) {
            echo "Opps! Email entered is not valid.";
            $noError = false;
        } elseif (!preg_match('/^[0-9]{10}+$/', $contactNumber)) {
            echo "Contact Number is not valid.";
            $noError = false;
        }

        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        if($check !== false) {
            $uploadOk = 1;
        } else {
            echo "File is not an image.";
            $uploadOk = 0;
        }

        // Check file size
        if ($_FILES["fileToUpload"]["size"] > 500000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }

        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $targetFile)) {
                $image = htmlspecialchars( basename( $_FILES["fileToUpload"]["name"]));
            } else {
            echo "Sorry, there was an error uploading your file.";
            }
        }

        if ($noError) {
            $header = "Image, Firstname, Lastname, Middlename, Gender, Contact Number, Email, Birthday, Address\n";
            $data = "$image, $firstname, $lastname, $middlename, $gender, $contactNumber, $email, $birthday, $address\n";
            $filename = dirname(__DIR__) . "/formdata-" . date("d-m-y") . ".csv";

            if (file_exists($filename)) {
                file_put_contents($filename, $data, FILE_APPEND);
                echo "Successfully saved to CSV.";
            } else {
                file_put_contents($filename, $header . $data);
                echo "Successfully saved to CSV.";
            }
        }
    }