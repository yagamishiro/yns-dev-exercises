<?php
    include '../database_conn.class.php';
    $connect = new DatabaseConnection('127.0.0.1','root','','sql_problems');
    $conn = $connect->connect();

    // $sql = "CREATE TABLE therapists_tbl (
    //     id INT(6) AUTO_INCREMENT PRIMARY KEY,
    //     name VARCHAR(255) NOT NULL
    //     )";
    $sql = "CREATE TABLE daily_work_shifts_tbl (
        id INT(6) AUTO_INCREMENT PRIMARY KEY,
        therapist_id INT(6) NOT NULL,
        target_date DATETIME NOT NULL,
        start_time DATETIME NOT NULL,
        end_time DATETIME NOT NULL
        )";

    if ($conn->query($sql) === TRUE) {
        echo "Table created successfully.";
    } else {
        echo "Error creating table : " . $conn->error;
    }

    $conn->close();
?>