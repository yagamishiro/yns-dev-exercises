<?php
    include '../database_conn.class.php';
    $connect = new DatabaseConnection('127.0.0.1','root','','sql_problems');
    $conn = $connect->connect();

    $sql = "INSERT INTO therapists_tbl (name)
    VALUES 
    ('John'),
    ('Arnold'),
    ('Robert'),
    ('Ervin'),
    ('Smith')
    ";
    
    if ($conn->query($sql) === TRUE) {
      echo "New record created successfully";
    } else {
      echo "Error: " . $sql . "<br>" . $conn->error;
    }
    
    $conn->close();

?>