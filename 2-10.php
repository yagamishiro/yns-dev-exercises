<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    </head>
<body style="height: 1000px;">
    <h1 class="text-center">Click button to go up or down</h1>
    <div style="margin-left: 45%;">
        <button type="button" class="btn btn-primary" onclick="goDown()">Go Down</button>

        <button type="button" class="btn btn-primary" style="position:absolute; margin-top:100%; margin-bottom: 5%;" onclick="goUp()">Go Up</button>
    </div>

    <script type="text/javascript">
        function goDown() {
            window.scrollTo(0,document.body.scrollHeight);
        }
        function goUp() {
            window.scrollTo(document.body.scrollHeight, 0);
        }
    </script>
</body>
</html>