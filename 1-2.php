<html>
    <body>
        <form method="POST">
        <input type="number" name="numberOne" placeholder="Input number 1"/>
        <select name="operation">
            <option value="add">Add</option>
            <option value="subtract">Subtract</option>
            <option value="multiply">Multiply</option>
            <option value="divide">Divide</option>
        </select>
        <input type="number" name="numberTwo" placeholder="Input number 2"/>
        <input type="submit" name="calculate" value="Submit" />
        </form>
    </body>

    <?php
        if (isset($_POST['calculate'])) {
            $numberOne = $_POST['numberOne'];
            $numberTwo = $_POST['numberTwo'];
            $operation = $_POST['operation'];
            $result = 0;
            
            switch ($operation) {
                case "add":
                    $result = $numberOne + $numberTwo;
                    echo "The result is ".$result;
                    break;
                case "subtract":
                    $result = $numberOne - $numberTwo;
                    echo "The result is ".$result;
                    break;
                case "multiply":
                    $result = $numberOne * $numberTwo;
                    echo "The result is ".$result;
                    break;
                case "divide":
                    $result = $numberOne / $numberTwo;
                    echo "The result is ".$result;
                    break;
                default:
                    echo "Error";
            }
        }
    ?>

</html>