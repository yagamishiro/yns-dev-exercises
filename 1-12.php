<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title></title>
  </head>
<body>
    <h1 class="text-center mt-5">User Informations</h1>
    <?php
        $csv = array();
        $newCsv = array();
        $filename = "formdata.csv";
        $header = true;
        echo '<table class="table table-bordered">';
        if (file_exists($filename)) {
            $handle = fopen($filename, "r");
            //display header row if true
            if ($header) {
                $csvcontents = fgetcsv($handle);
                echo '<tr>';
                foreach ($csvcontents as $headercolumn) {
                    echo "<th>$headercolumn</th>";
                }
                echo '</tr>';
            }
            // displaying contents
            while ($csvcontents = fgetcsv($handle)) {
                $csv = $csvcontents;
                array_push($newCsv, $csv);
            }
            $page = !empty($_GET['page']) ? (int)$_GET['page'] : 1;
            $total = count($newCsv);
            $limit = 10;
            $totalPages = ceil($total/$limit);
            $page = max($page, 1);
            $page = min($page, $totalPages);
            $offset = ($page - 1) * $limit;
            if($offset < 0 ) $offset = 0;
            $newCsv = array_slice($newCsv, $offset, $limit);
            foreach ($newCsv as $row) {
                echo '<tr>';
                echo "<td><img src=uploads/$row[0] width=150 height=150></td>";
                echo "<td>$row[1]</td>";
                echo "<td>$row[2]</td>";
                echo "<td>$row[3]</td>";
                echo "<td>$row[4]</td>";
                echo "<td>$row[5]</td>";
                echo "<td>$row[6]</td>";
                echo "<td>$row[7]</td>";
                echo "<td>$row[8]</td>";
                echo "<td>$row[9]</td>";
                echo '</tr>';
            }
            fclose($handle);
        } else {
            echo "<tr><td>No file exists ! </td></tr>" ;
        }
        echo '</table>';
        // echo '<pre>'; print_r(array_slice($new_csv, 0, 10)); echo '</pre>';
        $link = '1-12.php?page=%d';
        $pageContainer = "<div>";
        if ($totalPages != 0) {
            if ($page == 1) {
                $pageContainer .= "";
            } else {
                $pageContainer .= sprintf('<a href="' . $link . '" style=color: #c00"> &#171; Prev Page</a>', $page - 1);
            }
            $pageContainer .= ' <span> Page <strong>' . $page . '</strong> of ' . $totalPages . '</span>'; 
            if( $page == $totalPages ) 
            { 
                $pageContainer .= ''; 
            }
            else 
            { 
                $pageContainer .= sprintf( '<a href="' . $link . '" style="color: #c00"> Next Page &#187; </a>', $page + 1 ); 
            }            
        }
        $pageContainer .= '</div> <br>';

        echo $pageContainer;
    ?>
</body>
</html>