<html> 
    <body>
        <input type="number" id="number_one" placeholder="Input number 1"/>
        <select id="operation">
            <option value="add">Add</option>
            <option value="subtract">Subtract</option>
            <option value="multiply">Multiply</option>
            <option value="divide">Divide</option>
        </select>
        <input type="number" id="number_two" placeholder="Input number 2"/>
        <input type="button" value="Submit" onclick="calculate()" />
        <h1 id="result"></h1>
    </body>
    
    <script>
        function calculate() {
            var numberOne = parseInt(document.getElementById("number_one").value);
            var numberTwo = parseInt(document.getElementById("number_two").value);
            var operation = document.getElementById("operation").value;
            var res = 0;

            switch (operation) {
                case "add":
                    operation = "+"
                    res = numberOne + numberTwo;
                    break;
                case 'subtract':
                    operation = "-"
                    res = numberOne - numberTwo;
                    break;
                case 'multiply':
                    operation = "*"
                    res = numberOne * numberTwo;
                    break;
                case 'divide':
                    operation = "/"
                    res = numberOne / numberTwo;
                    break;
                default:
                    res = "Error.";
            }
            document.getElementById("result").innerHTML = numberOne + " " + operation + numberTwo + " = " + res;
        }
    </script>
</html>