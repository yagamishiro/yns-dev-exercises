<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    </head>
<body>
    <h1>Click button to change color</h1>
    <div style="padding: 10px;">
        <button type="button" class="btn btn-default" style="background-color: red; width: 100px;" onclick="changeColor('red')">Red</button>
        <button type="button" class="btn btn-default" style="background-color: blue; width: 100px;" onclick="changeColor('blue')">Blue</button>
        <button type="button" class="btn btn-default" style="background-color: yellow; width: 100px;" onclick="changeColor('yellow')">Yellow</button>
        <button type="button" class="btn btn-default" style="background-color: green; width: 100px;" onclick="changeColor('green')">Green</button>
    </div>

    <script type="text/javascript">
        function changeColor(color) {
            document.body.style.backgroundColor = color;
        }
    </script>
</body>
</html>