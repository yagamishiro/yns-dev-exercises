<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "exer_db";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql = "INSERT INTO user (firstname, lastname, email)
VALUES ('Rick', 'Sanchez', 'rick_sanchez@sample.com')";

if ($conn->query($sql) === TRUE) {
  echo "New record created successfully";
} else {
  echo "Error: " . $sql . "<br>" . $conn->error;
}

$conn->close();
?>