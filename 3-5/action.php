<?php
    session_start();

    $servername = "127.0.0.1";
    $username = "root";
    $password = "";
    $dbname = "3_5";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
    } 

    if (isset($_POST['submit'])) {
        
        $email = $_POST['email'];
        $password = $_POST['password'];
        $noError = true;

        if ($email == '' || $password == '') {
            $_SESSION['error'] = "You left one or more of the required fields.";
            header("Location:/yns-dev-exercises/3-5/index.php");
        }

        $sql = "SELECT * FROM user WHERE email = '$email' AND password = '$password'";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            $_SESSION['error'] = "Access Granted.";
            $_SESSION['logged_in'] = true;
            $_SESSION['email'] = $email;
            header("Location: /yns-dev-exercises/3-5/profile.php");
        } else {
            $_SESSION['error'] = "Sorry, that email or password is incorrect. Please try again.";
            header("Location: /yns-dev-exercises/3-5/index.php");
        }
    }

    if (isset($_POST['create_account'])) {
        $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $middlename = $_POST['middlename'];
        $gender = $_POST['gender'];
        $contactNumberExt = $_POST['contact_number_ext'];
        $contactNumber = $_POST['contact_number'];
        // $fullContactNumber = $contactNumberExt + $contactNumber;
        $email = $_POST['email'];
        $birthday = $_POST['birthday'];
        $address = $_POST['address'];
        $password = $_POST['password'];
        $confirmPassword = $_POST['confirmPassword'];
        $image = '';
        $noError = true;

        $targetDir = "../uploads/";
        $targetFile = $targetDir . basename($_FILES["fileToUpload"]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($targetFile,PATHINFO_EXTENSION));

        // Validate if fields required are empty
        if (
            $firstname == '' ||
            $lastname == '' ||
            $email == '' ||
            $birthday == '' ||
            $address == '' ||
            $contactNumber == '' ||
            $password == '' ||
            $confirmPassword == ''
        ) {
            $_SESSION['error'] = "You left one or more of the required fields.";
            header("Location:/yns-dev-exercises/3-5/create-account.php");
            $noError = false;
        } elseif (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $email)) {
            $_SESSION['error'] = "Email entered is not valid.";
            header("Location:/yns-dev-exercises/3-5/create-account.php");
            $noError = false;
        } elseif (!preg_match('/^[0-9]{10}+$/', $contactNumber)) {
            $_SESSION['error'] = "Contact Number is not valid.";
            header("Location:/yns-dev-exercises/3-5/create-account.php");
            $noError = false;
        }

        if ($password != $confirmPassword) {
            $_SESSION['error'] = "Password do not match.";
            header("Location:/yns-dev-exercises/3-5/create-account.php");
            $noError = false;
        }

        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        if($check !== false) {
            $uploadOk = 1;
        } else {
            echo "File is not an image.";
            $uploadOk = 0;
        }

        // Check file size
        if ($_FILES["fileToUpload"]["size"] > 500000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }

        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $targetFile)) {
                $image = htmlspecialchars( basename( $_FILES["fileToUpload"]["name"]));
            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }

        // if has no error insert into database
        if ($noError) {
            $sql = "INSERT INTO user (profile_pic, first_name, last_name, middle_name,
            gender, contact_number, birthday, `address`, email, `password`)
            VALUES ('$image','$firstname','$lastname','$middlename','$gender','$contactNumber','$birthday','$address','$email','$password')";

            if (mysqli_query($conn, $sql)) {
                $_SESSION['error'] = "Account succesfully created.";
                header("Location:/yns-dev-exercises/3-5/index.php");
            } else {
                $_SESSION['error'] = "Error: " . $sql . "<br>" . $conn->error;;
                header("Location:/yns-dev-exercises/3-5/index.php");
            }
        }
    }

    if (isset($_POST['logout'])) {
        session_start();
        header("Location:/yns-dev-exercises/3-5/index.php");
        session_destroy();
    }


?>