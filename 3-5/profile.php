<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title></title>
  </head>
<body>
    <?php 
        session_start();
        if (! empty($_SESSION['logged_in'])) {
    ?>
    <h1 class="text-center mt-5">User Profile</h1>
    <?php
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "3_5";
    
        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);
        // Check connection
        if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
        }
        
        $sql = "SELECT * FROM user WHERE email = '$_SESSION[email]'";
        $result = $conn->query($sql);

        echo '<table class="table table-bordered">';
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
            ?>
            <div class="container" style="width: 100%; border: 1px solid grey; border-radius: 30px; padding: 10px; overflow: hidden;">
                <div class="profileImage" style="float: left;">
                    <div style="margin-left:40%;">
                        <img src=../uploads/<?=$row['profile_pic']?> width=230 height=230 style="border-radius: 50%;">
                    </div>
                </div>
                <div class="userInfo">
                    <div style="margin-left:40%;">
                        <label style="font-size: 20px;">Name : </label>
                        <label style="font-weight: bold;"><?=$row['first_name'] . " " . $row['middle_name'] . ". " . $row['last_name']?></label>
                    </div>
                    <div style="margin-left:40%;">
                        <label style="font-size: 20px;">Gender : </label>
                        <label style="font-weight: bold;"><?=$row['gender']?></label>
                    </div>
                    <div style="margin-left:40%;">
                        <label style="font-size: 20px;">Birthday : </label>
                        <label style="font-weight: bold;"><?=$row['birthday']?></label>
                    </div>
                    <div style="margin-left:40%;">
                        <label style="font-size: 20px;">Address : </label>
                        <label style="font-weight: bold;"><?=$row['address']?></label>
                    </div>
                    <div style="margin-left:40%;">
                        <label style="font-size: 20px;">Contact Number : </label>
                        <label style="font-weight: bold;">0<?=$row['contact_number']?></label>
                    </div>
                    <div style="margin-left:40%;">
                        <label style="font-size: 20px;">Email Address : </label>
                        <label style="font-weight: bold;"><?=$row['email']?></label>
                    </div>
                </div>
            </div>
            <?php 
            }
        } ?>
        <br>
        <form method="POST" action="action.php">
            <button type="submit" class="btn btn-primary" name="logout" style="margin-left:48%;">Logout</button>
        </form>
        <?php } else { ?>
            <h3 class="text-center mt-5">You are not logged in. <a href="index.php">Click here</a> to log in.</h3>
        <?php } ?>
</body>
</html>