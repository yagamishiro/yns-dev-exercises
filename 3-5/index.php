<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title></title>
  </head>
  <body>
        <div style="width: 30%; margin: auto; border: 1px solid black; padding: 10px;" class="mt-5">
            <h1 class="text-center">Login Form</h1>
            <br>
            <span style="padding: 10px; color: red;">
                <?php
                    session_start();
                    if (isset($_SESSION['error'])) {
                        
                        echo $_SESSION['error'];
                        unset($_SESSION['error']);
                        session_destroy();
                    }
                ?>
            </span>
            <br>
            <br>
            <form method="POST" action="action.php">
                <div class="form-group">
                    <label for="email">Email address:</label>
                    <input type="email" class="form-control" placeholder="Enter email" name="email">
                </div>
                <div class="form-group">
                    <label for="pwd">Password:</label>
                    <input type="password" class="form-control" placeholder="Enter password" name="password">
                </div>
                <button type="submit" class="btn btn-primary" name="submit">Submit</button>
            </form>
            <div>
                <a href="create-account.php" style="margin-left: 70%;">Create Account</a>
            </div>
        </div>
  </body>
</html>