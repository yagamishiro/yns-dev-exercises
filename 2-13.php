<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    </head>
<body>
    <h1 class="text-center">Click button to change image size</h1>
    <div style="margin-left: 40%;">
        <img src="uploads/Rick_Sanchez.png" id="img1"/>

        <br><br>
        <button type="button" class="btn btn-primary" onclick="changeSize(150,150)">Small</button>
        <button type="button" class="btn btn-primary" onclick="changeSize(300,300)">Medium</button>
        <button type="button" class="btn btn-primary" onclick="changeSize(450,450)">Large</button>
    </div>
   
    <script type="text/javascript">
        function changeSize(width, height) {
            document.getElementById("img1").width = width;
            document.getElementById("img1").height = height;
        }
    </script>
</body>
</html>