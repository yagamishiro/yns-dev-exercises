<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    </head>
<body>
    <h1 class="text-center">Hover image to show another image</h1>
    <img src="uploads/Rick_Sanchez.png" id="img1" style="margin-left: 40%;"/>
    <img src="uploads/maxresdefault.jpg" style="width: 360px; height: 300px; margin-left: 40%; top: 100px; display: none;" id = "img2"/>
    <script type="text/javascript">
        var img1 = document.getElementById("img1"),
		img2 = document.getElementById("img2");

        img1.onmouseover = function() {
            img2.style.display = "block";
            img1.style.display = "none";
        }
        img1.onmouseout = function() {
            img2.style.display = "none";
            img1.style.display = "block";
        }
    </script>
</body>
</html>