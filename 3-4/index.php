<?php
$servername = "127.0.0.1";
$username = "root";
$password = "";
$dbname = "sql_problems";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

// last start with K
$sql = "SELECT * FROM employees WHERE last_name LIKE 'K%'";
$result = $conn->query($sql);

if ($result->num_rows > 0) { ?>
    <h3>1) Retrieve employees whose last name start with "K".</h3>
    <table style="width:50%; border: 1px solid black;
  border-collapse: collapse;">
            <th style="border: 1px solid black;
  border-collapse: collapse;">id</th>
            <th style="border: 1px solid black;
  border-collapse: collapse;">first_name</th>
            <th style="border: 1px solid black;
  border-collapse: collapse;">last_name</th>
            <th style="border: 1px solid black;
  border-collapse: collapse;">middle_name</th>
            <th style="border: 1px solid black;
  border-collapse: collapse;">department</th>
            <th style="border: 1px solid black;
  border-collapse: collapse;">hire_date</th>
            <th style="border: 1px solid black;
  border-collapse: collapse;">boss_id</th>
    <?php while($row = $result->fetch_assoc()) { ?>
        <tr>
            <td style="border: 1px solid black;
  border-collapse: collapse;"><?=$row["id"]?></td>
            <td style="border: 1px solid black;
  border-collapse: collapse;"><?=$row["first_name"]?></td>
            <td style="border: 1px solid black;
  border-collapse: collapse;"><?=$row["last_name"]?></td>
            <td style="border: 1px solid black;
  border-collapse: collapse;"><?=$row["middle_name"]?></td>
            <td style="border: 1px solid black;
  border-collapse: collapse;"><?=$row["department_id"]?></td>
            <td style="border: 1px solid black;
  border-collapse: collapse;"><?=$row["hire_date"]?></td>
            <td style="border: 1px solid black;
  border-collapse: collapse;"><?=$row["boss_id"]?></td>
        </tr>
    <?php } ?>
    </table> 
<?php } else {
    echo "0 results";
}

// lastname ends with i
$sql = "SELECT * FROM employees WHERE last_name LIKE '%i'";
$result = $conn->query($sql);

if ($result->num_rows > 0) { ?>
    <h3>2) Retrieve employees whose last name end with "i".</h3>
    <table style="width:50%; border: 1px solid black;
  border-collapse: collapse;">
            <th style="border: 1px solid black;
  border-collapse: collapse;">id</th>
            <th style="border: 1px solid black;
  border-collapse: collapse;">first_name</th>
            <th style="border: 1px solid black;
  border-collapse: collapse;">last_name</th>
            <th style="border: 1px solid black;
  border-collapse: collapse;">middle_name</th>
            <th style="border: 1px solid black;
  border-collapse: collapse;">department</th>
            <th style="border: 1px solid black;
  border-collapse: collapse;">hire_date</th>
            <th style="border: 1px solid black;
  border-collapse: collapse;">boss_id</th>
    <?php while($row = $result->fetch_assoc()) { ?>
        <tr>
            <td style="border: 1px solid black;
  border-collapse: collapse;"><?=$row["id"]?></td>
            <td style="border: 1px solid black;
  border-collapse: collapse;"><?=$row["first_name"]?></td>
            <td style="border: 1px solid black;
  border-collapse: collapse;"><?=$row["last_name"]?></td>
            <td style="border: 1px solid black;
  border-collapse: collapse;"><?=$row["middle_name"]?></td>
            <td style="border: 1px solid black;
  border-collapse: collapse;"><?=$row["department_id"]?></td>
            <td style="border: 1px solid black;
  border-collapse: collapse;"><?=$row["hire_date"]?></td>
            <td style="border: 1px solid black;
  border-collapse: collapse;"><?=$row["boss_id"]?></td>
        </tr>
    <?php } ?>
    </table> 
<?php } else {
    echo "0 results";
}

// hire date is between 2015/1/1 and 2015/3/31
$sql = "SELECT CONCAT(first_name, ' ', middle_name, ' ', last_name) AS full_name, hire_date FROM employees WHERE hire_date BETWEEN '2015-1-1' AND '2015-3-31' ORDER BY hire_date ASC";
$result = $conn->query($sql);

if ($result->num_rows > 0) { ?>
    <h3>3) Retrieve employee's full name and their hire date whose hire date is between 2015/1/1 and 2015/3/31 ordered by ascending by hire date.</h3>
    <table style="width:50%; border: 1px solid black;
  border-collapse: collapse;">
            <th style="border: 1px solid black;
  border-collapse: collapse;">full_name</th>
            <th style="border: 1px solid black;
  border-collapse: collapse;">hire_date</th>
    <?php while($row = $result->fetch_assoc()) { ?>
        <tr>
            <td style="border: 1px solid black;
  border-collapse: collapse;"><?=$row["full_name"]?></td>
            <td style="border: 1px solid black;
  border-collapse: collapse;"><?=$row["hire_date"]?></td>
        </tr>
    <?php } ?>
    </table> 
<?php } else {
    echo "0 results";
}

// retrieve employee and their boss last name
$sql = "SELECT employees.last_name AS employee, boss.last_name AS boss
FROM employees
INNER JOIN boss ON employees.boss_id = boss.id";

$result = $conn->query($sql);

if ($result->num_rows > 0) { ?>
    <h3>4) Retrieve employee's last name and their boss's last name. If they don't have boss, no need to retrieve and show.</h3>
    <table style="width:50%; border: 1px solid black;
  border-collapse: collapse;">
            <th style="border: 1px solid black;
  border-collapse: collapse;">Employee</th>
            <th style="border: 1px solid black;
  border-collapse: collapse;">Boss</th>
    <?php while($row = $result->fetch_assoc()) { ?>
        <tr>
            <td style="border: 1px solid black;
  border-collapse: collapse;"><?=$row["employee"]?></td>
            <td style="border: 1px solid black;
  border-collapse: collapse;"><?=$row["boss"]?></td>
        </tr>
    <?php } ?>
    </table> 
<?php } else {
    echo "0 results";
}

// employees last name that belongs to sales department
$sql = "SELECT last_name
FROM employees
INNER JOIN departments ON employees.department_id = departments.id
WHERE department_id = 3";

$result = $conn->query($sql);

if ($result->num_rows > 0) { ?>
    <h3>5) Retrieve employee's last name who belong to Sales department ordered by descending by last name.</h3>
    <table style="width:50%; border: 1px solid black;
  border-collapse: collapse;">
            <th style="border: 1px solid black;
  border-collapse: collapse;">last_name</th>
    <?php while($row = $result->fetch_assoc()) { ?>
        <tr>
            <td style="border: 1px solid black;
  border-collapse: collapse;"><?=$row["last_name"]?></td>
        </tr>
    <?php } ?>
    </table> 
<?php } else {
    echo "0 results";
}

// number of employee who has middle name
$sql = "SELECT COUNT(middle_name) AS count 
FROM employees
WHERE middle_name != '' AND middle_name IS NOT NULL";

$result = $conn->query($sql);

if ($result->num_rows > 0) { ?>
    <h3>6) Retrieve number of employee who has middle name.</h3>
    <table style="width:50%; border: 1px solid black;
  border-collapse: collapse;">
            <th style="border: 1px solid black;
  border-collapse: collapse;">count_has_middle_name</th>
    <?php while($row = $result->fetch_assoc()) { ?>
        <tr>
            <td style="border: 1px solid black;
  border-collapse: collapse;"><?=$row["count"]?></td>
        </tr>
    <?php } ?>
    </table> 
<?php } else {
    echo "0 results";
}

// retrieve department name and its number of employee
$sql = "SELECT COUNT(employees.department_id) AS count, departments.name AS name
FROM employees
INNER JOIN departments ON employees.department_id = departments.id
GROUP BY employees.department_id";

$result = $conn->query($sql);

if ($result->num_rows > 0) { ?>
    <h3>7) Retrieve department name and number of employee in each department. You don't need to retrieve the department name which doesn't have employee.</h3>
    <table style="width:50%; border: 1px solid black;
  border-collapse: collapse;">
            <th style="border: 1px solid black;
  border-collapse: collapse;">Name</th>
  <th style="border: 1px solid black;
  border-collapse: collapse;">count_employee_per_department</th>
    <?php while($row = $result->fetch_assoc()) { ?>
        <tr>
            <td style="border: 1px solid black;
  border-collapse: collapse;"><?=$row["name"]?></td>
            <td style="border: 1px solid black;
  border-collapse: collapse;"><?=$row["count"]?></td>
        </tr>
    <?php } ?>
    </table> 
<?php } else {
    echo "0 results";
}

// retrieve most recently hired
$sql = "SELECT first_name, middle_name, last_name, hire_date
FROM employees
ORDER BY hire_date DESC
LIMIT 1";

$result = $conn->query($sql);

if ($result->num_rows > 0) { ?>
    <h3>8) Retrieve employee's full name and hire date who was hired the most recently.</h3>
    <table style="width:50%; border: 1px solid black;
  border-collapse: collapse;">
            <th style="border: 1px solid black;
  border-collapse: collapse;">first_name</th>
  <th style="border: 1px solid black;
  border-collapse: collapse;">middle_name</th>
  <th style="border: 1px solid black;
  border-collapse: collapse;">last_name</th>
  <th style="border: 1px solid black;
  border-collapse: collapse;">hire_date</th>
    <?php while($row = $result->fetch_assoc()) { ?>
        <tr>
            <td style="border: 1px solid black;
  border-collapse: collapse;"><?=$row["first_name"]?></td>
            <td style="border: 1px solid black;
  border-collapse: collapse;"><?=$row["middle_name"]?></td>
  <td style="border: 1px solid black;
  border-collapse: collapse;"><?=$row["last_name"]?></td>
  <td style="border: 1px solid black;
  border-collapse: collapse;"><?=$row["hire_date"]?></td>
        </tr>
    <?php } ?>
    </table> 
<?php } else {
    echo "0 results";
}

// retrieve department with no employee
$sql = "SELECT departments.name AS name
FROM employees
RIGHT JOIN departments ON employees.department_id = departments.id
GROUP BY employees.department_id
HAVING COUNT(employees.department_id)<1";

$result = $conn->query($sql);

if ($result->num_rows > 0) { ?>
    <h3>9) Retrieve department name which has no employee.</h3>
    <table style="width:50%; border: 1px solid black;
  border-collapse: collapse;">
            <th style="border: 1px solid black;
  border-collapse: collapse;">name</th>
    <?php while($row = $result->fetch_assoc()) { ?>
        <tr>
            <td style="border: 1px solid black;
  border-collapse: collapse;"><?=$row["name"]?></td>
        </tr>
    <?php } ?>
    </table> 
<?php } else {
    echo "0 results";
}

// retrieve employees with more than 2 positions
$sql = "SELECT employees.first_name AS first_name, employees.middle_name, employees.last_name
FROM employees
INNER JOIN employee_positions ON employees.id = employee_positions.employee_id
GROUP BY employees.id
HAVING COUNT(employee_positions.employee_id) > 2";

$result = $conn->query($sql);

if ($result->num_rows > 0) { ?>
    <h3>10) Retrieve employee's full name who has more than 2 positions.</h3>
    <table style="width:50%; border: 1px solid black;
  border-collapse: collapse;">
            <th style="border: 1px solid black;
  border-collapse: collapse;">first_name</th>
            <th style="border: 1px solid black;
  border-collapse: collapse;">middle_name</th>
            <th style="border: 1px solid black;
  border-collapse: collapse;">last_name</th>
    <?php while($row = $result->fetch_assoc()) { ?>
        <tr>
            <td style="border: 1px solid black;
  border-collapse: collapse;"><?=$row["first_name"]?></td>
            <td style="border: 1px solid black;
  border-collapse: collapse;"><?=$row["middle_name"]?></td>
            <td style="border: 1px solid black;
  border-collapse: collapse;"><?=$row["last_name"]?></td>
        </tr>
    <?php } ?>
    </table> 
<?php } else {
    echo "0 results";
}

$conn->close();
?>