<?php
    $servername = "127.0.0.1";
    $username = "root";
    $password = "";
    $dbname = "sql_problems";

    $conn = new mysqli($servername, $username, $password, $dbname);

    if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
    }

    $sql = "CREATE TABLE employee_positions (
    id INT(6) AUTO_INCREMENT PRIMARY KEY,
    employee_id INT(6) NOT NULL,
    position_id INT(6) NOT NULL
    )";

    if ($conn->query($sql) === TRUE) {
    echo "Table created successfully.";
    } else {
    echo "Error creating table: " . $conn->error;
    }

    $conn->close();
?>