<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.5.0.js"></script>

        <style>
            @keyframes myAnimation {
                from{background-color: #00008B;}
                to{background-color: #ADD8E6;}
            }
        </style>
    </head>
<body onload="changeBackgroundColor()">
    <h1 class="text-center">Animation of changing background color from dark blue to light blue</h1>

    <script type="text/javascript">
        function changeBackgroundColor() {
            document.body.style.background = "#00008B";
            document.body.style.animation = "myAnimation 5s Infinite"
        }
    </script>
</body>
</html>