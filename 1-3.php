<html>
    
    <body>
        <form method="POST">
        <input type="number" name="numberOne" placeholder="Input number 1"/>
        <input type="number" name="numberTwo" placeholder="Input number 2"/>
        <input type="submit" name="calculate" value="Submit" />
        </form>
    </body>

    <?php
        if (isset($_POST['calculate'])) {
            $numberOne = $_POST['numberOne'];
            $numberTwo = $_POST['numberTwo'];
            
            function gcd($numberOne, $numberTwo)
            {
                if ($numberTwo == 0)
                    return $numberOne;
                return gcd($numberTwo, $numberOne%$numberTwo);
            }
            echo "GCD of $numberOne and $numberTwo is : ".gcd($numberOne, $numberTwo);
        }
    ?>

</html>