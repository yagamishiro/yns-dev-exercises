<?php
    if (isset($_POST['submit'])) {
        $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $middlename = $_POST['middlename'];
        $gender = $_POST['gender'];
        $contact_number_ext = $_POST['contact_number_ext'];
        $contact_number = $_POST['contact_number'];
        $email = $_POST['email'];
        $birthday = $_POST['birthday'];
        $address = $_POST['address'];
        $noError = true;

        // Validate if fields required are empty
        if (
            $firstname == '' ||
            $lastname == '' ||
            $email == '' ||
            $birthday == '' ||
            $address == '' ||
            $contact_number == ''
        ) {
            echo "Opps! Fields should not be empty.";
            $noError = false;
        } elseif (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $email)) {
            echo "Opps! Email entered is not valid.";
            $noError = false;
        } elseif (!preg_match('/^[0-9]{10}+$/', $contact_number)) {
            echo "Contact Number is not valid.";
            $noError = false;
        }

        // Display inputted data
        if ($noError) {
            echo "Inputted Basic User Information <br><br>";
            echo "Lastname : " . $lastname . "<br>";
            echo "Firstname : " . $firstname . "<br>";
            echo "Middlename : " . $middlename . "<br>";
            echo "Gender : " . $gender . "<br>";
            echo "Contact Number : " . $contact_number_ext . $contact_number . "<br>";
            echo "Email : " . $email . "<br>";
            echo "Birthday : " . $birthday . "<br>";
            echo "Address : " . $address . "<br>";
        }
    }