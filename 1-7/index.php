<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title></title>
  </head>
  <body>
        <form method="POST" action="display.php">
        <div class = "row">
            <div class="col-md-8 mx-auto">
            <h1>Basic User Information</h1>
            <label>Lastname : </label>
            <input type="text" name="lastname" placeholder="Input your lastname"/>
            <br>
            <label>Firstname : </label>
            <input type="text" name="firstname" placeholder="Input your firstname"/>
            <br>
            <label>Middlename(Optional) : </label>
            <input type="text" name="middlename" placeholder="Input your middlename"/>
            <br>
            <label>Gender : </label>
            <select name="gender">
                <option value="Male">Male</option>
                <option value="Female">Female</option>
            </select>
            <br>
            <label>Contact Number : </label>
            <input readonly type="text" name="contact_number_ext" value="+63" size="1"/>
            <input type="text" name="contact_number" placeholder="Input your contact number"/>
            <br>
            <label>Email : </label>
            <input type="text" name="email" placeholder="Input your email address"/>
            <br>
            <label>Birthday : </label>
            <input type="date" name="birthday" placeholder="Input your birthday"/>
            <br>
            <label>Address : </label>
            <input type="text" name="address" placeholder="Input your address"/>
            <br>
            <input type="submit" name="submit" value="Submit" />
            </div>
        </div>
        </form>
  </body>
</html>