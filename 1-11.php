<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title></title>
  </head>
<body>
    <h1 class="text-center mt-5">User Information</h1>
    <?php

        $csv = array();
        $filename = "formdata.csv";
        $header = true;
        echo '<table class="table table-bordered">';
        if (file_exists($filename)) {
            $handle = fopen($filename, "r");
            //display header row if true
            if ($header) {
                $csvcontents = fgetcsv($handle);
                echo '<tr>';
                foreach ($csvcontents as $headercolumn) {
                    echo "<th>$headercolumn</th>";
                }
                echo '</tr>';
            }
            // displaying contents
            while ($csvcontents = fgetcsv($handle, 1000, ",")) {
                $csv = $csvcontents;
                echo '<tr>';
                    echo "<td><img src=uploads/$csv[0] width=150 height=150></td>";
                foreach ($csv as $name => $column) {
                    if($name > 0)
                    echo "<td>$column</td>";
                }
                echo '</tr>';
            }
            fclose($handle);
        } else {
            echo "<tr><td>No file exists ! </td></tr>" ;
        }
        echo '</table>';
    ?>
</body>
</html>