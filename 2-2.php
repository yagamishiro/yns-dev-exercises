<!DOCTYPE html>
<html>
<body>

<h2>JavaScript Confirm Box</h2>


<button onclick="confirmDialog()">Press the Button</button>

<p id="message"></p>

<script>
function confirmDialog() {
  var txt;
  if (confirm("Press a button!")) {
    location.replace("redirect-page.php");
  } else {
    txt = "Remained on the page.";
  }
  document.getElementById("message").innerHTML = txt;
}
</script>

</body>
</html>
