<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    </head>
<body>
    <h1 class="text-center">Choose what image to display</h1>

    <div style="width: 100%; overflow: hidden;">
        
        <div style="width: 600px; float: left; margin-left: 30px;">
            <img src="uploads/Rick_Sanchez.png" id="img1" style="width: 500px; height: 500px;"/>
        </div>
        
        <div style="margin-top: 100px;">
            <label for="inputState">Choose image to show</label>
            <select id="imageChoice" class="form-control col-md-4" onchange="changeImage()">
                <option selected value="Rick_Sanchez.png">Rick Sanchez</option>
                <option value="maxresdefault.jpg">Simple Rick</option>
                <option value="morty.png">Morty Smith</option>
                <option value="jerry.jpg">Jerry Smith</option>
                <option value="beth.png">Beth Smith</option>
                <option value="summer.png">Summer Smith</option>
            </select>
        </div>
    </div>
   
    <script type="text/javascript">
        function changeImage() {
            let selected = document.getElementById("imageChoice").value;
            document.getElementById("img1").src="uploads/" + selected;
        }
    </script>
</body>
</html>